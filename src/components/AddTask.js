import {useState} from 'react';
import {connect} from 'react-redux';
import {addTodo} from '../store/todo';


function AddTask({addTodo}) {

    const [inputVal, setInputVal] = useState('');


    function addTask(){
        console.log(inputVal)
        if(inputVal){
            addTodo(inputVal)
            setInputVal('')
        }
    }
    return (
        <div>
           <div className="row">
               <div className="col-md-10">
                   <input type="text" className="form-control" value={inputVal} onChange={(e)=>setInputVal(e.target.value)}/>
               </div>
               <div className="col-md-2">
                   <button className="btn btn-outline-primary btn-block"  onClick={addTask}>Add</button>
               </div>
           </div>
        </div>
    )
}

export default connect(null, {addTodo})(AddTask)
