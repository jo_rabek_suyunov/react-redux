import {useState} from 'react';
import Todos from '../pages/Todos';
import Posts from '../pages/Posts';
import Users from '../pages/Users';
import Comments from '../pages/Comments';
import Photos from '../pages/Photos';
import Albums from '../pages/Albums';

import {Route, Switch, Link} from 'react-router-dom';

function App() {
    return (
        <div className={'container'}>
            <div className="row my-4">
                <div className="col-md-2">
                    <Link to={'/todos'}><button className={'btn btn-outline-primary btn-block'}>Todos</button></Link>
                </div>
                <div className="col-md-2">
                     <Link to={'/posts'}><button className={'btn btn-outline-info btn-block'}>Posts</button></Link>
                </div>
                <div className="col-md-2">
                     <Link to={'/users'}><button className={'btn btn-outline-info btn-block'}>Users</button></Link>
                </div>
                <div className="col-md-2">
                    <Link to={'/comments'}><button className={'btn btn-outline-primary btn-block'}>comments</button></Link>
                </div>
                <div className="col-md-2">
                     <Link to={'/albums'}><button className={'btn btn-outline-info btn-block'}>albums</button></Link>
                </div>
                <div className="col-md-2">
                     <Link to={'/photos'}><button className={'btn btn-outline-info btn-block'}>Photos</button></Link>
                </div>
            </div>
            <Switch>
                <Route path={'/todos'} component={Todos}/>
                <Route path={'/posts'} component={Posts}/>
                <Route path={'/users'} component={Users}/>
                <Route path={'/comments'} component={Comments}/>
                <Route path={'/albums'} component={Albums}/>
                <Route path={'/photos'} component={Photos}/>
            </Switch>
            
            
        </div>
    )
}

export default App
