import React from 'react';
import {toggleTodo} from '../store/todo';
import {connect} from 'react-redux';

function Todo({item, toggleTodo}) {
    return (
        <div  key={item.id} className={'row'}>
                <div className="col-md-2">
                    <input type="checkbox" checked={item.complete} onChange={()=>toggleTodo(item.id)}  id={"check/" + item.id} className={'checkbox'}/>
                </div>
                <div className="col-md-10">
                  <label htmlFor={"check/"+ item.id} className={'label'}>
                     {item.title}
                  </label>
                </div>
        </div>
    )
}

export default connect(null, ({toggleTodo}))(Todo);
