import {useEffect} from 'react';
import {connect} from 'react-redux';
import { getTodosFromBack } from '../store/todo';
import Todo from './Todo';

function TaskList({todos, getTodosFromBack}) {
    

    useEffect(()=>{
        getTodosFromBack()
    }, [])
    return (
        <div>
            {
                todos.map((item)=><Todo key={item.id} item={item}/>)
            }
            
        </div>
    )
}

export default connect(({todos: {todos}})=>({todos}), {getTodosFromBack})(TaskList)
