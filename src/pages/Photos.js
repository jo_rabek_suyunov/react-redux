import {useEffect} from 'react';
import {getPhotos} from '../store/photos';
import {connect} from 'react-redux';

function Photos({photos, getPhotos}) {
    useEffect(()=>{
        getPhotos();
    }, [])
    return (
        <div>
            <h1>Photos</h1>
            {
                photos.map((photo, index)=><div key={index}>
                  <h1>  {photo.id}. {photo.title}</h1>
                </div>)
            }
            
        </div>
    )
}

export default connect(({photos:{photos}})=>({photos}), {getPhotos})(Photos);


