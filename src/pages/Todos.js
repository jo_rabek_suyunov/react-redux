import React from 'react';
import AddTask from '../components/AddTask';
import TaskList from '../components/TaskList';

function Todos() {
    return (
        <div className="row">
                <div className="col-md-6 offset-3">
                    <div className="card">
                        <div className="card-header">
                            <AddTask/>
                        </div>
                        <div className="card-body">
                            <TaskList/>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default Todos
