import {useEffect} from 'react';
import {getComments} from '../store/comments';
import {connect} from 'react-redux';

function Comments({comments, getComments}) {
    console.log(comments)

    useEffect(()=>{
        getComments()
    }, [])


    return (
        <div>
            <h1>Comments</h1>
            <div className="row">
                {
                    comments.map((comment, index)=><div className={'col-md-4'}>
                        <div className="card">
                            <div className="card-header">
                                №: {comment.id}
                                <br/>
                              <h1>  {comment.name}</h1>

                            </div>
                            <div className="card-body">
                               <p>
                               {comment.body}
                               </p>

                            </div>
                            <div className="card-footer">
                                <h3>{comment.email}</h3>

                            </div>
                        </div>

                    </div>)
                }
            </div>
            
        </div>
    )
}

export default connect(({comments:{comments}})=>({comments}), {getComments})(Comments);
