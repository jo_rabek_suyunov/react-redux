import {useEffect} from 'react';
import {connect} from 'react-redux';
import {getAlbums} from '../store/albums';

function Albums({albums, getAlbums}) {
    console.log(albums)

    useEffect(()=>{
        getAlbums()
    }, [])
    
    return (
        <div>
            <h1 align={'center'}>Albums</h1>
            <table className="table">
                <thead>
                    <tr>
                        <th>userId</th>
                        <th>Id</th>
                        <th>Title</th>
                    </tr>
                </thead>
                <tbody>
                  {
                      albums.map((album, index)=><tr key={album.id}>
                          <td>{album.id}</td>
                          <td>{album.userId}</td>
                          <td>{album.title}</td>

                      </tr>)
                      
                  }
                </tbody>
            </table>
            
        </div>
    )
}

export default connect(({albums: {albums}})=>({albums}), {getAlbums})(Albums);
