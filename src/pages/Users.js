import {useEffect} from 'react';
import {connect} from 'react-redux';
import {getUsers} from '../store/users';


function Users({users, getUsers}) {

    useEffect(()=>{
        getUsers()
    }, [])
    return (
        <div>
            <h1  align="center">Users</h1>
            <table className="table">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>name</th>
                        <th>username</th>
                        <th>email</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        users.map((user, index)=><tr>
                            <td>{user.id}</td>
                            <td>{user.name}</td>
                            <td>{user.username}</td>
                            <td>{user.email}</td>
                        </tr>)
                    }
                </tbody>
            </table>
            
        </div>
    )
}




export default connect(({users:{users}})=>({users}), {getUsers})(Users);
