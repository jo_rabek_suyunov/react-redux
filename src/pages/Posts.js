import {useEffect} from 'react';
import {connect} from 'react-redux';
import {getPosts} from '../store/post';

function Posts({posts, getPosts}) {

    useEffect(()=>{
        getPosts()

    }, [])
    return (
        <div>
            {
                posts.map((item)=><p  key={item.id}> <b>{item.id}</b>: {item.title}</p>)
            }
            
        </div>
    )
}

export default connect(({posts: {posts}})=>({posts}), {getPosts})(Posts);
