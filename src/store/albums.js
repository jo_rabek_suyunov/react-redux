import axios from 'axios';
import {createSlice} from '@reduxjs/toolkit';

 export function getAlbums(){
    return (dispatch)=>{
        axios.get('http://jsonplaceholder.typicode.com/albums').then(res=>{
          dispatch({
              type: getAlbum.type,
              payload: res.data
          })
        })
    }
}



// export default function albumReducer(state = {
//     albums: []
// }, action){
//     switch(action.type){
//         case 'GET_ALBUMS':
//             state = {
//                 ...state,
//                 albums: action.payload
//             }
//             break;
//     }

//     return state;

// }


const album = createSlice({
    name: 'albums',
    initialState: {albums: []},
    reducers: {
        getAlbum: (state, action)=>{
            state.albums = action.payload
        }
    }
})

export const {getAlbum} = album.actions;
export default album.reducer;