import axios from 'axios';
import {createSlice} from '@reduxjs/toolkit';

export function getPhotos(){
    return (dispatch)=>{
        axios.get('http://jsonplaceholder.typicode.com/photos').then(res=>{
           dispatch({
               type: getPhoto.type,
                payload: res.data
           })
        })
    }
}


const photo = createSlice({
    name: 'photos',
    initialState: { photos:[]},
    reducers: {
        getPhoto: (state, action)=>{
            state.photos = action.payload
        }
    }
});

export const {getPhoto} = photo.actions;
export default photo.reducer;
