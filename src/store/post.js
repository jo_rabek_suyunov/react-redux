import axios from 'axios';
import {createSlice} from '@reduxjs/toolkit';

//  const GET_POSTS = 'GET_POSTS';
//  const ADD_POST = 'ADD_POST';

export function getPosts(){
    return (dispatch)=>{
        axios.get('https://jsonplaceholder.typicode.com/posts').then(res=>{
            dispatch({
                type: getPost.type,
                payload: res.data
            })
        })

    }
}



// export default function postsReducer(state = {
//     posts: []
// }, action){
//     switch (action.type) {
//         case GET_POSTS:
//             state = {
//                 ...state,
//                 posts: action.payload
//             }
//             break;
//        
      
//     }

//     return state;

// }


const a = createSlice({
    name: 'posts',
    initialState:{ posts: []},
    reducers: {
        getPost: (state, action)=>{
           state.posts = action.payload
        }

    }
});


export const {getPost} = a.actions;
export default a.reducer;