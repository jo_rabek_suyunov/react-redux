import axios from 'axios';
import {createSlice} from '@reduxjs/toolkit';

export function getUsers(){
    return (dispatch)=>{
        axios.get('https://jsonplaceholder.typicode.com/users').then(res=>{
            dispatch({
                type: getUser.type,
                payload: res.data
            })
        })

    }
}


const users = createSlice({
    name: 'users',
    initialState: { users: [] },
    reducers:{
        getUser: (state, action)=>{
            state.users = action.payload

        }

    }
})

export const {getUser} = users.actions;

export default users.reducer;
