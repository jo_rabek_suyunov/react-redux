
import todoReducer from './todo';
import postReducer from './post';
import userReducer from './users';
import commentsReducer from './comments';
import albumReducer from './albums';
import photosReducer from './photos';
import { configureStore } from '@reduxjs/toolkit';



export default configureStore({
    reducer: {
        todos: todoReducer, 
        posts: postReducer,
        users: userReducer,
        comments: commentsReducer,
        albums: albumReducer,
        photos: photosReducer
    }
})