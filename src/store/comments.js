import axios from 'axios';
import {createSlice} from '@reduxjs/toolkit';


 export function getComments(){
     return (dispatch) =>{
        axios.get('http://jsonplaceholder.typicode.com/comments').then(res=>{
           dispatch({
               type: getComment.type,
               payload: res.data
           })
       
        })
     }

}


const comment = createSlice({
    name: 'comments',
    initialState: {comments:[]},
    reducers:{
        getComment: (state, action) => {
            state.comments = action.payload
        }
    }
})


export const {getComment} = comment.actions;
export default comment.reducer;
