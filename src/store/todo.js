import axios from 'axios';
import {createAction, createReducer, createSlice} from '@reduxjs/toolkit';


export function getTodosFromBack(){
    return (dispatch, getState)=>{
        //console.log(getState()) state ni ham olishimiz mumkin.
        axios.get('https://jsonplaceholder.typicode.com/todos').then(res=>{
        dispatch ({
            type: getTodosAction.type,
            payload: res.data
        })
    })
    }
}



const a = createSlice({
    name: 'todos',
    initialState: {todos:[]},
    reducers: {
        getTodosAction: (state, action)=>{
            state.todos = action.payload
         },
         addTodo: (state, action)=>{
             state.todos.unshift({id: state.todos.length+1, title: action.payload, complete: false})
            
         },
         toggleTodo: (state, action)=>{
             state.todos.map((todo)=>{
                 if(todo.id === action.payload){
                     todo.complete = !todo.complete
                 }
             })
           
         }

    }
})

export const {getTodosAction, addTodo, toggleTodo} = a.actions;
export default a.reducer;
